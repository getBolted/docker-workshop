package shutdown

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

var gc *gracefulShutdown

type application interface {
	Run(ctx context.Context) error
	Stop()
}

func GC() *gracefulShutdown {
	return gc
}

type gracefulShutdown struct {
	isAlreadyStopping bool
	app               application
	log               *slog.Logger
	done              chan struct{}
	signal            chan os.Signal
}

func InitShutdown(log *slog.Logger, app application) {
	gc = &gracefulShutdown{log: log, signal: make(chan os.Signal), done: make(chan struct{}), app: app}
}

func (gc *gracefulShutdown) Halt() {
	if !gc.isAlreadyStopping {
		gc.signal <- syscall.SIGTERM
	}
}

func (gc *gracefulShutdown) Monitor(cancelRunApp context.CancelFunc) {
	go func(f context.CancelFunc) {
		gc.log.Debug("Shutdown monitor started")
		signal.Notify(gc.signal, syscall.SIGINT, syscall.SIGTERM)

		y := <-gc.signal
		gc.log.With(slog.String("signal", y.String())).Info("Received signal, initializing shutdown ops...")
		gc.isAlreadyStopping = true
		f()
		gc.app.Stop()
		close(gc.done)
	}(cancelRunApp)
}

func (gc *gracefulShutdown) Wait() {
	<-gc.done
}
