package config

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/jinzhu/configor"
)

const (
	EnvProduction = "PRODUCTION"
	EnvPrefix     = "DEMO"
)

type Config struct {
	Environment  string       `yaml:"environment" env:"DEMO_ENVIRONMENT" default:"development"`
	ServiceName  string       `yaml:"service_name" env:"DEMO_SERVICE_NAME" default:"demo-server"`
	AnswerLength int          `yaml:"answer_length" env:"DEMO_ANSWER_LENGTH" default:"5"`
	Server       ServerConfig `yaml:"server"         env:"-"`
}

type ServerConfig struct {
	HttpServerPort int `yaml:"http_server_port" env:"DEMO_HTTP_SERVER_PORT" default:"8080"`
}

func NewConfig(path string) (*Config, error) {
	var cfg Config
	loader := configor.New(&configor.Config{ENVPrefix: EnvPrefix, ErrorOnUnmatchedKeys: true})
	if err := loader.Load(&cfg, path); err != nil {
		return nil, fmt.Errorf("failed to load config: %w", err)
	}

	if cfg.Environment != EnvProduction {
		spew.Dump(cfg)
	}

	return &cfg, nil
}
