FROM golang:alpine
WORKDIR /build
COPY . .
RUN go build -o demo-server ./cmd/demo/main.go
CMD ["./demo-server"]
