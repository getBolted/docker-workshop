package ports

type Demo interface {
	GenerateRandomAnswer() (string, error)
}
