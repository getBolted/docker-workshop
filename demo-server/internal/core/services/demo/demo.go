package demo

import (
	"demo-server/internal/ports"
	"log/slog"
	"math/rand"
	"strings"
)

const (
	charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

var _ ports.Demo = (*Service)(nil)

type Service struct {
	answerLen int
	logger    *slog.Logger
}

func NewService(answerLen int, logger *slog.Logger) *Service {
	return &Service{answerLen: answerLen, logger: logger}
}

func (s *Service) GenerateRandomAnswer() (string, error) {
	sb := strings.Builder{}
	sb.Grow(s.answerLen)
	for i := 0; i < s.answerLen; i++ {
		sb.WriteByte(charset[rand.Intn(len(charset))])
	}
	return sb.String(), nil
}
