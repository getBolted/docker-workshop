package models

type DemoResponse struct {
	Answer string `json:"answer"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}
