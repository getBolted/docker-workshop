package internal

import (
	"context"
	"demo-server/internal/adapter/rest"
	"demo-server/internal/core/services/demo"
	"demo-server/pkg/config"
	"log/slog"
	"sync"
)

type Application struct {
	log    *slog.Logger
	cfg    *config.Config
	server *rest.Server
}

func NewApplication(log *slog.Logger, conf *config.Config) *Application {
	return &Application{log: log, cfg: conf}
}

func (a *Application) Run(ctx context.Context) error {
	service := demo.NewService(a.cfg.AnswerLength, a.log)
	a.server = rest.NewServer(a.log, service, a.cfg.Server.HttpServerPort)

	a.server.Run()

	return nil
}

func (a *Application) Stop() {
	wg := &sync.WaitGroup{}

	if a.server != nil {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			a.log.Debug("Shutting down HTTP server")
			a.server.Stop()
		}(wg)
	}

	wg.Wait()
	a.log.Info("All dependencies are down, stopping...")
}
