package handlers

import (
	"context"
	"demo-server/internal/core/models"
	"demo-server/internal/ports"
	"encoding/json"
	"log/slog"
	"net/http"
)

type DemoHandler struct {
	svc ports.Demo
	log *slog.Logger
}

func NewDemoHandler(svc ports.Demo, log *slog.Logger) *DemoHandler {
	return &DemoHandler{svc: svc, log: log}
}

func (d *DemoHandler) DemoHandle(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		d.log.Error("wrong method", slog.String("method", r.Method))
		resp := models.ErrorResponse{Error: "wrong method used"}
		d.wJsonResponse(r.Context(), resp, w, http.StatusBadRequest)

		return
	}

	d.log.Info("Got new request")

	str, err := d.svc.GenerateRandomAnswer()
	if err != nil {
		d.log.Error("unable to generate answer", slog.String("err_desc", err.Error()))
		resp := models.ErrorResponse{Error: "unable to generate answer"}
		d.wJsonResponse(r.Context(), resp, w, http.StatusInternalServerError)

		return
	}

	resp := models.DemoResponse{Answer: str}
	d.log.Info("Answering with", slog.String("answer", str))
	d.wJsonResponse(r.Context(), resp, w, http.StatusOK)
}

func (d *DemoHandler) wJsonResponse(ctx context.Context, response interface{}, w http.ResponseWriter, code int) {
	var jsonData []byte

	if response != nil && response != http.NoBody {
		w.Header().Set("Accept", "application/json")

		var err error
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		jsonData, err = json.Marshal(response)
		if err != nil {
			d.log.Error("error occurred during marshal result", slog.String("err_decr", err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	d.wResponse(ctx, jsonData, w, code)
}

func (d *DemoHandler) wResponse(ctx context.Context, resp []byte, w http.ResponseWriter, code int) {
	w.WriteHeader(code)

	if len(resp) > 0 {
		_, err := w.Write(resp)
		if err != nil {
			d.log.Error("failed to rest response", slog.String("err_decr", err.Error()))
		}
	}
}
