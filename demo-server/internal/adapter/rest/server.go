package rest

import (
	"context"
	"demo-server/internal/adapter/rest/handlers"
	"demo-server/internal/ports"
	"demo-server/pkg/shutdown"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"
)

const (
	shutdownTimeout = 30 * time.Second
)

type Server struct {
	log        *slog.Logger
	httpServer *http.Server
	svc        ports.Demo
}

func NewServer(log *slog.Logger, svc ports.Demo, httpPort int) *Server {
	server := &http.Server{
		Addr: fmt.Sprintf(":%d", httpPort),
	}
	return &Server{log: log, httpServer: server, svc: svc}
}

func (s *Server) Run() {
	handle := handlers.NewDemoHandler(s.svc, s.log)

	mux := http.NewServeMux()
	mux.HandleFunc("/random", handle.DemoHandle)
	s.httpServer.Handler = mux

	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			s.log.Error("failed to start rest server", slog.String("err_desc", err.Error()))
			shutdown.GC().Halt()
		}
	}()
}

func (s *Server) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer cancel()

	if err := s.httpServer.Shutdown(ctx); err != nil {
		s.log.Error("shutdown failed", slog.String("err_desc", err.Error()))
	}
}
