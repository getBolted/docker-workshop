FROM golang:1.21 as builder

WORKDIR /app
COPY ../. .

ENV CGO_ENABLED=0
ENV GOOS=linux

RUN go build -o demo-server ./cmd/demo/main.go

FROM alpine:latest

COPY --from=builder /app/demo-server /bin/demo-server

ENTRYPOINT ["demo-server"]

EXPOSE 8080