module demo-server

go 1.21.2

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/jinzhu/configor v1.2.1
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
