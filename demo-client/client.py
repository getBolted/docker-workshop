import os
import sys
from datetime import datetime
import signal
import requests
from time import sleep


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        self.kill_now = True


if __name__ == '__main__':
    ADDR = os.getenv('DEMO_ADDR')
    FILEPATH = os.getenv('DEMO_FILEPATH')

    if ADDR is None:
        print("ADDR ENV has not been set")
        sys.exit(1)

    if FILEPATH is None:
        print("FILEPATH ENV has not been set")
        sys.exit(1)

    killer = GracefulKiller()
    while not killer.kill_now:
        sleep(2)
        try:
            r = requests.get(ADDR)
            resp = r.json()['answer']
            print(resp)
            with open(FILEPATH, 'a') as fp:
                fp.write(f'{datetime.now()} : {resp}\n')
        except requests.exceptions.ConnectionError:
            print("Unable to connect")

    print("Done, exiting")

